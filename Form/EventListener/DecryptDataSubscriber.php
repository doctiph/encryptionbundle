<?php
/**
 * User: mminasyan
 * Date: 03/03/14
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */

namespace Encryption\Bundle\EncryptionBundle\Form\EventListener;


use Encryption\Bundle\EncryptionBundle\Entity\EncryptableEntityInterface;
use Encryption\Bundle\EncryptionBundle\Manager\EncryptorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DecryptDataSubscriber implements EventSubscriberInterface
{
    /**
     * @var \Encryption\Bundle\EncryptionBundle\Manager\EncryptorInterface
     */
    private $encryptor;

    /**
     * List of encrypted fields by entity
     * @var array
     */
    private $fields;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $isMcryptActive;

    public function __construct(EncryptorInterface $encryptor, $fields, $isMcryptActive, LoggerInterface $logger)
    {
        $this->encryptor = $encryptor;
        $this->fields = $fields;
        $this->isMcryptActive = $isMcryptActive;
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SUBMIT => 'submit');
    }

    public function submit(FormEvent $event)
    {
        $entity = $event->getForm()->getData();

        if ($this->isMcryptActive && $entity instanceof EncryptableEntityInterface) {
            $data = $event->getData();

            $class = strtolower(substr(get_class($entity), strrpos(get_class($entity), '\\')+1));
            $data  = $this->decryptData($class, $data);

            $event->setData($data);
        }
    }

    /**
     * @param $class
     * @param $data
     * @return mixed
     */
    private function decryptData($class, $data)
    {
        $this->logger->info('Decrypting data for class '.$class);
        $fields = $this->fields[$class];

        foreach ($fields as $field) {

            if (isset($data[$field])) {

                if (is_array($data[$field])) {
                    $data[$field] = $this->decryptData($field, $data[$field]);
                } elseif (!empty($data[$field])) {
                    $data[$field] = $this->encryptor->decrypt($data[$field]);
                }
            }
        }

        return $data;
    }
}