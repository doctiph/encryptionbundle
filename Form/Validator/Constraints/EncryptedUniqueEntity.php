<?php

namespace Encryption\Bundle\EncryptionBundle\Form\Validator\Constraints;

/**
 * User: mminasyan
 * Date: 07/03/14
 * Time: 15:18
 * To change this template use File | Settings | File Templates.
 */

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

class EncryptedUniqueEntity extends UniqueEntity
{
    public $service = 'mcrypt.validator.unique';
}