<?php

namespace Encryption\Bundle\EncryptionBundle\Form\Validator;

/**
 * User: mminasyan
 * Date: 07/03/14
 * Time: 12:31
 * To change this template use File | Settings | File Templates.
 */
use Encryption\Bundle\EncryptionBundle\Manager\CanonicalizerInterface;
use Encryption\Bundle\EncryptionBundle\Manager\EncryptorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

class EncryptedUniqueEntityValidator extends ConstraintValidator
{
    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * @var EncryptorInterface
     */
    private $enryptor;

    /**
     * @var CanonicalizerInterface
     */
    private $canonicalizer;

    private $isEncryptionOn;

    public function __construct(ManagerRegistry $registry, EncryptorInterface $encryptor, CanonicalizerInterface $canonicalizer, $isEncryptionOn)
    {
        $this->registry = $registry;
        $this->enryptor = $encryptor;
        $this->canonicalizer = $canonicalizer;
        $this->isEncryptionOn = $isEncryptionOn;
    }

    public function validate($entity, Constraint $constraint)
    {
        if (!is_array($constraint->fields) && !is_string($constraint->fields)) {
            throw new UnexpectedTypeException($constraint->fields, 'array');
        }

        if (null !== $constraint->errorPath && !is_string($constraint->errorPath)) {
            throw new UnexpectedTypeException($constraint->errorPath, 'string or null');
        }

        $fields = (array) $constraint->fields;

        if (0 === count($fields)) {
            throw new ConstraintDefinitionException('At least one field has to be specified.');
        }

        if ($constraint->em) {
            $em = $this->registry->getManager($constraint->em);
        } else {
            $em = $this->registry->getManagerForClass(get_class($entity));
        }

        $className = $this->context->getClassName();
        $class = $em->getClassMetadata($className);

        $criteria = array();
        foreach ($fields as $fieldName) {
            if (!$class->hasField($fieldName) && !$class->hasAssociation($fieldName)) {
                throw new ConstraintDefinitionException(sprintf("The field '%s' is not mapped by Doctrine, so it cannot be validated for uniqueness.", $fieldName));
            }

            $value = $this->canonicalizer->canonicalize($class->reflFields[$fieldName]->getValue($entity));

            if ($this->isEncryptionOn) {
                $value = $this->enryptor->encrypt($value);
            }

            $criteria[$fieldName] = $value;

            if ($constraint->ignoreNull && null === $criteria[$fieldName]) {
                return;
            }

            if ($class->hasAssociation($fieldName)) {
                /* Ensure the Proxy is initialized before using reflection to
                 * read its identifiers. This is necessary because the wrapped
                 * getter methods in the Proxy are being bypassed.
                 */
                $em->initializeObject($criteria[$fieldName]);

                $relatedClass = $em->getClassMetadata($class->getAssociationTargetClass($fieldName));
                $relatedId = $relatedClass->getIdentifierValues($criteria[$fieldName]);

                if (count($relatedId) > 1) {
                    throw new ConstraintDefinitionException(
                        "Associated entities are not allowed to have more than one identifier field to be " .
                        "part of a unique constraint in: ".$class->getName()."#".$fieldName
                    );
                }
                $criteria[$fieldName] = array_pop($relatedId);
            }
        }

        $repository = $em->getRepository($className);
        $result = $repository->{$constraint->repositoryMethod}($criteria);

        /* If the result is a MongoCursor, it must be advanced to the first
         * element. Rewinding should have no ill effect if $result is another
         * iterator implementation.
         */
        if ($result instanceof \Iterator) {
            $result->rewind();
        } elseif (is_array($result)) {
            reset($result);
        }

        /* If no entity matched the query criteria or a single entity matched,
         * which is the same as the entity being validated, the criteria is
         * unique.
         */
        if (0 === count($result) || (1 === count($result) && $entity === ($result instanceof \Iterator ? $result->current() : current($result)))) {
            return;
        }

        $errorPath = null !== $constraint->errorPath ? $constraint->errorPath : $fields[0];

        $this->context->addViolationAt($errorPath, $constraint->message, array(), $criteria[$fields[0]]);
    }
}