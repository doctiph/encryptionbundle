<?php
/**
 * User: mminasyan
 * Date: 03/03/14
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */

namespace Encryption\Bundle\EncryptionBundle\Manager;


interface EncryptorInterface
{
    /**
     * Must accept secret key for encryption
     */
    public function __construct($secretKey, $algorithm, $mode);

    /**
     * Must accept data and return encrypted data
     */
    public function encrypt($data);

    /**
     * Must accept data and return decrypted data
     */
    public function decrypt($data);
}