<?php
/**
 * User: mminasyan
 * Date: 07/03/14
 * Time: 18:15
 * To change this template use File | Settings | File Templates.
 */

namespace Encryption\Bundle\EncryptionBundle\Manager;


class Canonicalizer implements CanonicalizerInterface
{
    public function canonicalize($string)
    {
        return null === $string ? null : mb_convert_case($string, MB_CASE_LOWER, mb_detect_encoding($string));
    }
}