<?php

namespace Encryption\Bundle\EncryptionBundle\Manager;

/**
 * User: mminasyan
 * Date: 07/03/14
 * Time: 18:15
 * To change this template use File | Settings | File Templates.
 */
interface CanonicalizerInterface
{
    /**
     * @param string $string
     *
     * @return string
     */
    public function canonicalize($string);
}