<?php
/**
 * User: mminasyan
 * Date: 03/03/14
 * Time: 15:56
 * To change this template use File | Settings | File Templates.
 */

namespace Encryption\Bundle\EncryptionBundle\Manager;


class McryptManager implements EncryptorInterface
{
    private $secretKey;
    private $algorithm;
    private $mode;

    public function __construct($secretKey, $algorithm, $mode)
    {
        $this->secretKey = pack('H*', $secretKey);
        $this->algorithm = $algorithm;
        $this->mode      = $mode;
    }

    public function encrypt($data)
    {
        return trim(base64_encode(mcrypt_encrypt($this->algorithm, $this->secretKey, $data, $this->mode, $this->_generateIv())));
    }

    public function decrypt($data)
    {
        return trim(mcrypt_decrypt($this->algorithm, $this->secretKey, base64_decode($data), $this->mode, $this->_generateIv()));
    }

    private function _generateIv()
    {
        return mcrypt_create_iv(mcrypt_get_iv_size($this->algorithm, $this->mode), MCRYPT_RAND);
    }
}