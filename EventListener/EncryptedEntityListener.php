<?php
/**
 * User: mminasyan
 * Date: 04/03/14
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */

namespace Encryption\Bundle\EncryptionBundle\EventListener;


use Encryption\Bundle\EncryptionBundle\Entity\EncryptableEntityInterface;
use Encryption\Bundle\EncryptionBundle\Manager\CanonicalizerInterface;
use Encryption\Bundle\EncryptionBundle\Manager\EncryptorInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use FOS\UserBundle\Model\User;
use Psr\Log\LoggerInterface;

class EncryptedEntityListener
{
    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * List of encrypted fields by entity
     * @var array
     */
    private $fields;

    private $isApi;
    private $isMcryptActive;

    /**
     * @var CanonicalizerInterface
     */
    private $canonicalizer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EncryptorInterface $encryptor, $isMcryptActive, $fields, $isApi, CanonicalizerInterface $canonicalizer, LoggerInterface $logger)
    {
        $this->encryptor = $encryptor;
        $this->isMcryptActive = $isMcryptActive;
        $this->fields = $fields;
        $this->isApi = $isApi;
        $this->canonicalizer = $canonicalizer;
        $this->logger = $logger;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $class  = strtolower(substr(get_class($entity), strrpos(get_class($entity), '\\')+1));

        if ($this->isMcryptActive && $entity instanceof EncryptableEntityInterface && isset($this->fields[$class])) {
            $this->logger->info('Encrypting data for class '.$class.' on prePersist');
            $fields = $this->fields[$class];

            if ($entity instanceof User) {
                $entity->setEmailCanonical($this->encryptor->encrypt($this->canonicalizer->canonicalize($entity->getEmail())));
                $entity->setUsernameCanonical($this->encryptor->encrypt($this->canonicalizer->canonicalize($entity->getUsername())));
            }

            foreach ($fields as $field) {
                $setter = 'set'.ucfirst($field);
                $getter = 'get'.ucfirst($field);

                if (method_exists($entity, $setter) && method_exists($entity, $getter)) {
                    $getterValue = $entity->$getter();

                    if (!is_object($getterValue) && !empty($getterValue)) {
                        $entity->$setter($this->encryptor->encrypt($getterValue));
                    }
                }
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        $class  = strtolower(substr(get_class($entity), strrpos(get_class($entity), '\\')+1));

        if ($this->isMcryptActive && $entity instanceof EncryptableEntityInterface && isset($this->fields[$class])) {
            $this->logger->info('Encrypting data for class '.$class.' on preUpdate');
            $fields = $this->fields[$class];

            if ($entity instanceof User) {
                if ($args->hasChangedField('email')) {
                    $args->setNewValue('emailCanonical', $this->encryptor->encrypt($this->canonicalizer->canonicalize($args->getNewValue('email'))));
                }
                if ($args->hasChangedField('username')) {
                    $args->setNewValue('usernameCanonical', $this->encryptor->encrypt($this->canonicalizer->canonicalize($args->getNewValue('username'))));
                }
            }

            foreach ($fields as $field) {
                if ($args->hasChangedField($field)) {
                    $newValue = $args->getNewValue($field);
                    if (!empty($newValue)) {
                        $args->setNewValue($field, $this->encryptor->encrypt($newValue));
                    }
                }
            }
        }
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $class  = strtolower(substr(get_class($entity), strrpos(get_class($entity), '\\')+1));

        if ($this->isMcryptActive && !$this->isApi && $entity instanceof EncryptableEntityInterface && isset($this->fields[$class])) {
            $this->logger->info('Decrypting data for class '.$class.' on postLoad');
            $fields = $this->fields[$class];

            foreach ($fields as $field) {
                $setter = 'set'.ucfirst($field);
                $getter = 'get'.ucfirst($field);
                if (method_exists($entity, $setter) && method_exists($entity, $getter)) {
                    $getterValue = $entity->$getter();

                    if (!is_object($getterValue) && !empty($getterValue)) {
                        $entity->$setter($this->encryptor->decrypt($getterValue));
                    }
                }
            }
        }
    }

}