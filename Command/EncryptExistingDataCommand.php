<?php
/**
 * User: mminasyan
 * Date: 25/03/14
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */

namespace Encryption\Bundle\EncryptionBundle\Command;


use Encryption\Bundle\EncryptionBundle\Entity\EncryptableEntityInterface;
use La\ElleUserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EncryptExistingDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('mcrypt:data:encyptAll')
            ->setDescription('Encrypts all data from database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container      = $this->getContainer();
        $doctrine       = $container->get('doctrine');
        $encryptor      = $container->get('mcrypt.manager');
        $canonicalizer  = $container->get('mcrypt.canonicalizer');
        $logger         = $container->get('logger');
        $encryptableFields = $container->getParameter('mcrypt_fields');

        $crms   = $doctrine->getRepository("LaElleUserBundle:Crm")->findAll();
        $users  = $doctrine->getRepository("LaElleUserBundle:User")->findAll();
        $addresses = $doctrine->getRepository("DoctipharmaAccountBundle:Address")->findAll();

        $data = array_merge($crms, $addresses, $users);

        foreach ($data as $entity) {
            $class  = strtolower(substr(get_class($entity), strrpos(get_class($entity), '\\')+1));

            if ($entity instanceof EncryptableEntityInterface && isset($encryptableFields[$class])) {
                $logger->info('Encrypting data for class '.$class.' in command');
                $fields = $encryptableFields[$class];

                if ($entity instanceof User) {
                    $entity->setEmailCanonical($encryptor->encrypt($canonicalizer->canonicalize($entity->getEmail())));
                    $entity->setUsernameCanonical($encryptor->encrypt($canonicalizer->canonicalize($entity->getUsername())));
                }

                foreach ($fields as $field) {
                    $setter = 'set'.ucfirst($field);
                    $getter = 'get'.ucfirst($field);

                    if (method_exists($entity, $setter) && method_exists($entity, $getter)) {
                        $getterValue = $entity->$getter();

                        if (!is_object($getterValue) && !empty($getterValue)) {
                            $entity->$setter($encryptor->encrypt($getterValue));
                        }
                    }
                }
            }
        }

        $doctrine->getManager()->flush();
    }
}