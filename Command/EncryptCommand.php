<?php
/**
 * User: mminasyan
 * Date: 25/03/14
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */

namespace Encryption\Bundle\EncryptionBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EncryptCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('mcrypt:data:encrypt')
            ->setDescription('Encrypt a data')
            ->addArgument(
                'data',
                InputArgument::REQUIRED,
                'data to encrypt'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->getContainer()->get('mcrypt.manager')->encrypt($input->getArgument('data')));
    }
}